package helpers;

import java.util.ArrayList;

public class Explainer {
	String featureList[] = {
	"Average incloseness centrality",
	"Average Betweenness centrality",
	"Average Page Rank",
	"Average Degree",
	"Average Weighted Degree",
	"Average Degree Overlap",
	"Avg. Clustering Coefficient",
	"Avg Risk Index",
	"Max Degree",
	"Min Degree",
	"MaxDegree/MinDegree",
	"Max Clustering Coefficient",
	"Min Clustering Coefficient",
	"Max Risk Index",
	"Min Risk Index",
	"MaxRIskIndex/MinRiskIndex",
	"max weighted degree",
	"min weighted degree",
	"max weighted degree/min weighted degree",
	"Cluster value"};
	
	ParetoFrontier pareto;
	double UserSol[], OptSol[];
	DecisionTree bestDtree;
	boolean bestDtreeFound;
	
	ArrayList<DecisionTree> feasibleTrees;
	
	DecisionTree getMostInterpretableTree()
	{
		DecisionTree result=null;
		double maxInterpret = -1;
		int maxIndex = -1;
		for (int i=0;i<feasibleTrees.size();i++)
		{
			if (feasibleTrees.get(i).interpretability>maxInterpret)
			{
				maxInterpret = feasibleTrees.get(i).interpretability;
				maxIndex = i;
			}
		}
		if (maxIndex!=-1)
		{	
			result = feasibleTrees.get(maxIndex);
			bestDtreeFound=true;
		}
		
		return result;
	}
	
	public Explainer(double UserSol[], double OptSol[], boolean isProbabilityMethod)
	{
		bestDtreeFound = false;
		pareto = new ParetoFrontier();
		this.UserSol = UserSol;
		this.OptSol = OptSol;
		
		feasibleTrees = new ArrayList<DecisionTree>();
		for (int i=0;i<pareto.list.length;i++)
		{
			DecisionTree curr = pareto.list[i];
			int predictionOnUserSol=-1, predictionOnOptSol=-1;
			if (isProbabilityMethod)
			{
				double pred1 = predictDouble(UserSol, curr);
				double pred2 = predictDouble(OptSol, curr);
				
				if (pred2>=pred1)
				{
					predictionOnOptSol=1;
					predictionOnUserSol=0;
				}
				else
				{
					predictionOnOptSol=0;
					predictionOnUserSol=1;
				}
			}
			else
			{
				predictionOnUserSol = predict(UserSol, curr);
				predictionOnOptSol = predict(OptSol, curr);
			}
			
			
			if (predictionOnUserSol==0 && predictionOnOptSol==1)
				feasibleTrees.add(curr);
		}
		
		if (feasibleTrees.size()==0)
			System.out.println("No feasible trees found");
		
		
		bestDtree = getMostInterpretableTree();

	}
	
	int predict(double featureVec[], DecisionTree tree)
	{
		Node curr = tree.root;
		while (curr.isLeaf==false)
		{
			if (featureVec[curr.feature-1]<curr.splitVal)
				curr = curr.successors[0];
			else
				curr = curr.successors[1];
		}
		
		if (curr.classProb>0.5)
			return 1;
		else
			return 0;
	}
	
	double predictDouble(double featureVec[], DecisionTree tree)
	{
		Node curr = tree.root;
		while (curr.isLeaf==false)
		{
			if (featureVec[curr.feature-1]<curr.splitVal)
				curr = curr.successors[0];
			else
				curr = curr.successors[1];
		}
		
		return curr.classProb;
	}
	
	public String provideFirstDivergenceExplanation()
	{
		String result="";
		if (bestDtreeFound)
		{
			Node tree = bestDtree.root;
			while(tree.isLeaf==false)
			{
				if (UserSol[tree.feature-1]<tree.splitVal && OptSol[tree.feature-1]<tree.splitVal)
					tree = tree.successors[0];
				else if (UserSol[tree.feature-1]>=tree.splitVal && OptSol[tree.feature-1]>=tree.splitVal)
					tree = tree.successors[1];
				else//we have a divergence
				{
					result +="Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]+". ";
					System.out.println("Your solution had a "+featureList[tree.feature-1]+" value of "+UserSol[tree.feature-1]);
					result += "On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]+". ";
					System.out.println("On the other hand, the optimal solution had a "+featureList[tree.feature-1]+" value of "+OptSol[tree.feature-1]);
					if (UserSol[tree.feature-1]<tree.splitVal)
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be greater than or equal to "+tree.splitVal);
					}
					else
					{
						result += "For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal+". ";
						System.out.println("For solutions to be optimal, the value of "+featureList[tree.feature-1]+" should be strictly less than "+tree.splitVal);
					}
					break;
				}
			}
			
		}
		else
		{
			System.out.println("No decision tree found which separates optimal from user solution.");
		}
		return result;
	}
	
	void provideCompleteExplanation()
	{
		//Use with changed notion of maximum interpretability...now most interpretable tree is one 
		//in which the divergence occurs at the deepest level...i.e. closest to the leaves...so that
		//number of features that have to be explained is lesser
	}
}
