<?php
//session_set_cookie_params(0);
session_start();
if(!isset($_SESSION['passcode']))
{
	session_write_close();
    header("Location:index.php");
}
else
{
    if($_SESSION['visitedSampleGame'] == false)
    {
        $_SESSION['visitedSampleGame'] = true;
		session_write_close();
        header("Location:SampleGame.php");
    }
    else
    {
        $user_responses = $_SESSION['user_responses'];
		session_write_close();
		header("Location:/GraphValidation.php?" . $user_responses);
    }
        
}
//include 'Graphgame.php';
//include 'logoff.html';

?>