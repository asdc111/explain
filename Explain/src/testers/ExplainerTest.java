package testers;

import java.util.Random;

import helpers.CreateSolutions;
import helpers.Explainer;

public class ExplainerTest {
	public static void main(String[] args) {
		Random rand = new Random();
		int userSol1 = rand.nextInt(20);
		int userSol2 = rand.nextInt(20);
	
		CreateSolutions sols = new CreateSolutions(1, userSol1, userSol2);
		
		Explainer explain = new Explainer(sols.UserSol, sols.OptSol, true);
		String explanation = explain.provideFirstDivergenceExplanation();
		System.out.println("This is the explanation: ");
		System.out.println(explanation);
	}
}
