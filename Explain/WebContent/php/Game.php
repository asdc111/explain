<? ob_start();?>
<?php
//session_set_cookie_params(0);
session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

if(isset($_SESSION['passcode']) )
{
    //header("Location:content.php");
}
else
{
     header("Location:index.php");
}

if(!isset($_SESSION['counter']))
{
	$_SESSION['counter']=0;
	$_SESSION['jsonfile'] = array();
	$_SESSION['NoOfGraphs'] = 0 ;
	$_SESSION['user_responses'] = '';
	$handle = fopen("./AMTGraphs/experimentMetadata.txt", "r");
	if ($handle) 
	{
		while (($line = fgets($handle)) !== false) {
						$_SESSION['NoOfGraphs']++;
						$_SESSION['jsonfile'][] = explode(",", $line);
					}
		
		fclose($handle);
		
		//Randomize the graphs
		shuffle($_SESSION['jsonfile']);
	}
	else 
	{
		echo "Error reading the metadata file";
					// error opening the file.
	} 
}
//echo "counter=". $_SESSION['counter'];

/*
$time = microtime();
$time = explode(' ', $time);
$time = $time[1] + $time[0];
$start = $time;

echo '<script language="javascript">';
echo 'alert(" hai " )';
echo '</script>'*/
?>
<!--<!DOCTYPE html> -->
<html>

<head>
  <meta charset="utf-8">
  <link href="bootstrap.min.css" rel="stylesheet">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Teamcore Network Security Game</title>
  <link rel="stylesheet" href="app.css">
<script src="respond.min.js"></script>
	<meta http-equiv="X-UA-Compatible" content="chrome=1">
	
</head>

<body>
<script type="text/javascript" src="jquery-1.11.1.min.js"></script>
<script type="text/javascript">
                flag=true;
                
                window.onbeforeunload = function(){
                    if(flag)
                    {
                         $.get("logout.php");
                        //return "Warning you are still logged in, are you sure?";
                    }
                    };
</script>


<STYLE>H3 {FONT-SIZE: 21pt; COLOR: #FFFFFF; background-color:#000;}; H3 {FONT-SIZE: 17pt; COLOR: #FFFFFF}</STYLE>
<!--[if IE ]>
<H3>Special instructions for IE here</H3>
<![endif]-->
 
<!--
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	
			<div class="navbar-header">
-->
<table style="width: 100%; border:0" cellpadding="0" ,="" cellspacing="0">
    <tbody>
    <!--    <tr style="background:#000"><td colspan="2" style="padding-right:80px; height:60px" align="right">
        <a href="http://www.usc.edu">
            <img height="65" src="/images/gateway-usc-shield-name.png" border="0" alt="University of Southern California"></a></td>
        </tr>-->
        <tr style="height:80px; background:#900"><td style="padding-left:80px">
        <a href="http://viterbi.usc.edu/"><img src="TeamcoreRG_RGB_Form_horiz-04.png" height="70" border="0" alt="Viterbi School of Engineering"></a>
        </td><td style="padding-right:100px" align="right"><img src="TeamcoreRG_PMS_wordmark-05.png" height="50" border="0" alt="teamcore"></td></tr></tbody></table>
  
        
        
    <form action="<?php $_PHP_SELF ?>" method="post" onsubmit="flag=false;">

     <!--   <img src="logo.jpg">-->
     <!--      <a href="logout.php" style="vertical-align: bottom" >logout</a></br> -->
        <br/>
        <br/>
<!--			<h3 >
			  TeamCore Networked security Game
			 </h3>

			</div>
			
		</div>
-->

			<div id='chartline1' class="col-md-8">
				</div>
            <div class="col-md-4">
			<br/>
			  <h3>Game Round</h3>
             <br/>
			 <a class="button" href=InstructionsPopup.php target="_new">Instructions</a>
					  <br/>
            <ol>
						  <li>Select 2 nodes by clicking on nodes of your choice.</li>
						  <li>In order to change your selection, click on any node to unselect it. You will be able to click the submit button only after you select 2 nodes.</li>
                </ol>
                <b><label >Your current selection : </label> <input hidden="true" style="border:none" id="selected-path" name="selected-path" readonly="readonly" onkeypress="this.style.width = ((this.value.length + 1) * 8) + 'px';"></input><br/>
					  <label hidden="true"></label><input  hidden="true" id="selected-path-cost" style="border:none" name="selected-path-cost" readonly="readonly"></input> <br/>

                <label></label><input id="selected-path-reward" style="border:none" name="selected-path-reward" readonly="readonly"></input> </b><br/>
    
                <input type="submit" name="submitgame" id="submit" value="Submit" disabled=true/>
                </div>
                
</form>
    
    <?php
    
    
		include_once "JSON.php";
        $exec_time = 0;
        if (isset($_POST['submitgame']))
//Also check document.getElementById('submit').disabled == true
       {
           $time = microtime();
           $time = explode(' ', $time);
           $time = $time[1] + $time[0];
           $finish = $time;
           $exec_time = round(($finish - $_SESSION['starttime']), 4);
           $_SESSION['starttime'] = $time;
           //echo 'Page generated in '.$exec_time.' seconds.';
           //$exec_time = $time_post-$time_pre;
           //$exec_time = $time_post - $time_pre;

		   array2csv( $exec_time);
		   $_SESSION['counter']=$_SESSION['counter']+1;   

		   if($_SESSION['counter'] == $_SESSION['NoOfGraphs'])
		   {
		   		header("Location:InstructionsValid.php");
				//header("Location:ThankYou.php");
		   }
		   else
		   {
				header("Location:Game.php");//Prevents incorrect resubmission of form data after a page refresh.
		   }
		   
       }        
       else
       {
	   
           $time = microtime();
           $time = explode(' ', $time);
           $time = $time[1] + $time[0];
           $_SESSION['starttime'] = $time;
           

       }
        function array2csv($exec_time)
        {
            
            $json = new Services_JSON();
            $gameSuccess = true;
            $path = $_POST['selected-path'];
            //$cost = $_POST['selected-path-cost'];
            //$reward = $_POST['selected-path-reward'];
            
            /*$coveragedata = file_get_contents("./AMTGraphs/" . trim($_SESSION['jsonfile'][$_SESSION['counter']][1], " \t\n\r\0\x0B"));
            $coveragedata = utf8_encode($coveragedata);

            $coverage_a = $json->decode($coveragedata,TRUE);
            $Delimiter = '"';
            $Separator = ',';
            //$coverage_a = json_decode($coveragedata,true);
            //var_dump($coverage_a->Coverage);
            foreach($coverage_a->Coverage as $cover )
            {
                
                 if ((strpos($path,$cover->id) !== false)) 
                 {
						 if($cover->covered == true)
						 {
							 $gameSuccess = false;
						 }
                    
                }
            }
			if($gameSuccess == false)
			{
				$gameSuccess=0;
				$reward =0;
			}*/
            
			//Format: SessionID, Round Number, GraphFilepath, user's chosen attack path, success?, reward, time played
			$list = array (
			array($_SESSION['passcode'],$_SESSION['jsonfile'][$_SESSION['counter']][0],"Round".$_SESSION['counter'],$path , $exec_time));
			$filename = "./file_shortcertain.csv";
			$newFile = false;
			
			if($_SESSION['counter'] != $_SESSION['NoOfGraphs']) {
				$_SESSION['user_responses'] .= "round".$_SESSION['counter'] ."=" . $_SESSION['jsonfile'][$_SESSION['counter']][0] . ':' . $path . '&';
			} else {
				$_SESSION['user_responses'] .= "round".$_SESSION['counter'] . "=" . $_SESSION['jsonfile'][$_SESSION['counter']][0] . ':' . $path;
			}

			if (!file_exists($filename))
			{
				$newFile = true;
			}

			$fp= fopen("./file_shortcertain.csv", "a");
			
			foreach ($list as $fields) {
				//Code adapted from http://stackoverflow.com/questions/16942531/alternative-to-fputcsv
				//Code block uses an alternative to fputcsv if the current version of PhP does not define it.
				if (!function_exists('fputcsv')){
					 fwrite($fp, $Delimiter.
							implode($Delimiter.$Separator.$Delimiter, $fields).$Delimiter."\n");
				}
				else
				{
					fputcsv($fp, $fields);
				}
			}

			fclose($fp);
			if($newFile)
			{
				chmod($fp, 0666);
			}
        }

        ?>
     <?php



                $jsondata = file_get_contents("./AMTGraphs/".$_SESSION['jsonfile'][$_SESSION['counter']][0]);
              
                 echo '<script>var jsondata = ' . $jsondata. ';</script>';

              
     ?>
    
  </body>

<script src="d3.v3.min.js"></script>
<script src="d3-grid.js"></script>
<script src="app.js"></script>


</html>
<? ob_flush(); ?>