package helpers;

public class ParetoFrontier {
	public DecisionTree list[];
	
	public ParetoFrontier()
	{
		String basedir = "/Users/amulyayadav/Dropbox/Work/PostAAMAS/EceProject/Clustering/5PhebeRegression/FinalDataset/normalized-extended/v1NetNum/Output/ParetoTrees/CSV/CSV-Copy/";
		///there are 33 trees in the pareto frontier
		list = new DecisionTree[33];
		for (int i=0;i<33;i++)
		{
			list[i] = new DecisionTree(basedir+"dtree"+(i+1)+".csv");
		}
	}
}
