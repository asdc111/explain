package helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CreateSolutions {
	public double UserSol[];
	public double OptSol[];
	
	String solutiondir ="/Users/amulyayadav/Desktop/Data/SolutionsDir/";
	///last line of solution file will be optimal solution
	public CreateSolutions(int netNumber, int A, int B)
	{
		try 
		{
			int index=0;
			Scanner sc1 = new Scanner(new File(solutiondir+"POMDPSols/"+netNumber+".txt"));
			int pomdp1=sc1.nextInt();
			int pomdp2=sc1.nextInt();
			
			System.out.println("User Solution: "+A+" "+B);
			System.out.println("POMDP Solution: "+pomdp1+" "+pomdp2);
		
			
			boolean usersol_set = false, optsol_set=false;
			
			Scanner sc = new Scanner(new File(solutiondir+"UserSols/"+netNumber+".txt"));
			while(sc.hasNextLine())
			{
				String solutionNodes[] = sc.nextLine().split("\\s+");
				int first = Integer.parseInt(solutionNodes[0]);
				int second = Integer.parseInt(solutionNodes[1]);
				if (!usersol_set && ((first==A && second==B) || (first==B && second==A)))
				{
					usersol_set=true;
					UserSol = new double[solutionNodes.length-2];
					for (int i=2;i<solutionNodes.length;i++)
						UserSol[i-2]= Double.parseDouble(solutionNodes[i]);
				}
				
				if (!optsol_set && ((first==pomdp1 && second==pomdp2) || (first==pomdp2 && second==pomdp1)))
				{
					optsol_set=true;
					OptSol = new double[solutionNodes.length-2];
					for (int i=2;i<solutionNodes.length;i++)
						OptSol[i-2]= Double.parseDouble(solutionNodes[i]);
				}
				
				if (usersol_set && optsol_set)
					break;
			}
			
			
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}
	}
}
