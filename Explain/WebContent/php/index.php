<?php
//session_set_cookie_params(0);
session_start();

if(isset($_SESSION['passcode']) && isset($_SESSION['subjectPool']))
{
    //header("Location:content.php");	
	header("Location:".$_SESSION['subjectPool']);
}
?>
<html>
    <head>
		<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="../images/favicon.ico" type="image/x-icon">
        <title>Teamcore Network Security Game</title>
		<script src="../js/browserCheck.js"></script>
    </head>
    <body>
        <img src="../images/logo.jpg">
        <br/>
        <br/>
		<h3><font color="red"><p id="browserWarning"></p></font></h3>
        
            <form method="post" action="login.php">
               <!-- 
                <table>
                    <tr>
                        <td> Enter email ID:</td>
                        <td><input type="text" name="eid"></td>
                    </tr>
                    <tr>
                        <td> Password:</td>
                        <td><input type="=password" name="password"></td>
                        
                    </tr>
                    <tr>
                        <td><input type="submit" name="login" value="Login"</td>
                      
                    </tr>
                </table>-->
            
            
    <table>
        <tr>
            <td><h4>Email address (Optional):</h4></td>
            <td><input type="text" name="formemail" value=""></td>
        </tr>
        <tr>
            <td><h4>Gender (Optional):</h4></td>
            <td><input type="radio" id="maleGender" name="formGender" value="Male">Male<br>
	            <input type="radio" id="femaleGender" name="formGender" value="Female">Female<br>
				<input type="radio" id="otherGender" name="formGender" value="Other">Other (please specify)
				<input type="text" id="otherGenderField" name="formGenderText" value=""><br>
            </td>
        </tr>
        <tr>
            <td><h4>Age:</h4></td>
            <td><select name="formAge">
                <option value="NotSet" selected="selected">Select a range of your age</option>
                <option value="20 or under">20 or under</option>
                <option value="21 - 25">21 - 25</option>
                <option value="26 - 30">26 - 30</option>
                <option value="31 - 35">31 - 35</option>
                <option value="36 - 40">36 - 40</option>
                <option value="41 or above">41 or above</option>
                </select>
            </td>
        </tr>
    <table>
	
	<br>

	<br>
	<input id="submitButton" type="submit" name="formSubmit" value="Next">
	</form>
	<!--Browser detection script-->
	<script type="text/javascript">
		//If the browser type is IE
		if(navigator.sayswho == 'IE')
		{
			//Make the browser warning visible.
			document.getElementById("browserWarning").innerHTML = "This game is not compatible with Internet Explorer. Please use a browser such as Google Chrome or Mozilla Firefox.";
			
			//Also, disable the submit button functionality.
			document.getElementById("submitButton").disabled = "true";
		}
	</script>
    </body>
</html>