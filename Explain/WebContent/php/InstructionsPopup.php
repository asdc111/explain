<? ob_start();?>
<?php
//session_set_cookie_params(0);
/*session_start();
if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
    // last request was more than 30 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time 
    session_destroy();   // destroy session data in storage
}
$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp
if(isset($_SESSION['passcode']) )
{
    //header("Location:content.php");
}
else
{
     header("Location:index.php");
}

    
if($_SESSION['visitedSampleGame']) 
    $BtnLabel ='Start the Game'; 
else 
    $BtnLabel ='Start Sample Game'; 
*/
?>

<html>

<head>
  <title>Teamcore Network Security Game</title>
  <meta charset=utf-8 />
  <style>
    div.container {
      display:inline-block;
    }


  </style>
</head>
</head>
<body>
    <img src="logo.jpg">
    <script type="text/javascript" src="jquery-1.11.1.min.js"></script>
    <!--<script type="text/javascript">
                flag=true;
                window.onbeforeunload = function(){
                    if(flag)
                    {      $.get("logout.php");
                      //  return "Warning you are still logged in, are you sure?";
                    }
                    };
</script> -->
       <!-- <form action="content1.php" method="post" onsubmit="flag=false;"> -->
<H1>Maximizing Influence in a Single Step Under No Uncertainty</H1>
<H2><u>Context</u></H2>
<p align="justify">Homeless youth are at high risk from HIV-AIDS (the dangerous disease) because of activities like sharing drug needles etc., which are very common among homeless youth. Thus, many social workers (and associated agencies) are planning to conduct an intervention camp for homeless youth in order to raise awareness 
about HIV among homeless youth. However, since social workers are few in number, they can only reach out to a couple of homeless youth in their planned intervention camp. They plan to encourage attendees of their intervention camp to raise awareness about HIV among all their peers. Therefore, the social workers have 
to decide which homeless youth should be invited to participate in their intervention camp. Intuitively, they want to invite the <it>"most influential"</it> homeless youth, i.e., youth who will be able to raise awareness among as many homeless youth as possible (or youth who will maximize the spread of awareness 
about HIV). To aid them in this decision, they have been given information about a social network which shows all the friendships between all the homeless youth. By looking at this social network (which will be described next), the social workers have to decide which youth from this network 
should be invited for their intervention. Since our social workers are exhausted from a tough day at work, they have requested you to make this decision for them. </p>


<H2><u>Social Network Description</u></H2>
<p align="justify">The social network represents a map of friendships between different homeless youth. In this social network, each homeless youth is represented by a circle similar to the one shown in the figure below. The letter inside the circle represents the name of that particular homeless youth. For example, the name of the
homeless youth in the figure is <b>M</b>. Moreover, each friendship in the social network is represented by a solid line that connects two circles. For example, the figure below shows a line between two circles <b>B</b> and <b>C</b>, which represents that the homeless youth corresponding to circles <b>B</b> and <b>C</b> are friends.
</p> 

<div class="container">
<img src="./images/GameScreenshots/SourceNode.png" />
<p align="center">Network Node</p>
</div>
<div class="container">
<img src="./images/GameScreenshots/edge.png" />
<p align="center">Network Edge</p>
</div>





<H2><u>Influence Model</u></H2>
<p align="justify">In order to help you in your task, the social workers tell you how the attendees of their intervention camp will raise awareness among their peers. According to them, if circle <b>E</b> is invited for the intervention, then he/she will raise awareness about HIV among all homeless youth which are at most two hops away from
him in the social network. In other words, the invited attendee will raise awareness among (i) all his friends; and (ii) all the friends of his friends. For example, the figure below shows an example of a network in which circle <b>E</b> was invited for the intervention (denoted by the red color of circle <b>E</b>), then he/she will
be able to raise awareness among the set of homeless youth which are denoted by green circles. These green circles represent all homeless youth which are at most two hops away from the red <b>E</b> circle.</p>  


<div class="container">
<img src="./images/GameScreenshots/influ-mod1.png" />
<p align="center">Sample Network</p>
</div>
<div class="container">
<img src="./images/GameScreenshots/influ-mod2.png" />
<p align="center">Influence spread by circle E</p>
</div>


<H2><u>Goal</u></H2>
<p align="justify">In order to assist the social workers, your goal is to select two homeless youth in the network. You should select the two homeless youth in a way such that the overall number of homeless youth who get aware about HIV (according to the influence model described above) is maximized. In order to clearly explain the objective, 
we consider a random selection of two homeless youth (or circles) in the social network shown in the figure below. In this figure, by selecting circles <b>E</b> and <b>D</b>, overall four youth were made aware about HIV (shown by green circles). In addition, circles <b>E</b> and <b>D</b> were obviously aware about HIV as they attended
our intervention camps. Therefore, by selecting circles <b>E</b> and <b>D</b>, we were able to raise awareness among a total of 4+2 = 6 circles (or youth). Your goal is to select two youth (or circles) in the social network which will maximize the number of youth who are made aware about HIV in the social network.</p>

<div class="container">
<img src="./images/GameScreenshots/goal.png" />
<p align="center">Random Selection of two nodes and Awareness Spread</p>
</div>

<H2><u>Instructions</u></H2>
<p align="justify">You can select a circle by clicking on it using your mouse. The color of a circle changes to red when it is selected. If you don't like your selection, and would like to modify it, you can unselect a selected node by clicking on it again. Nodes which are unselected will have white color. When you are happy with you selection, 
you can press the Submit button in order to submit your response. First, you will be shown a sample network in order to aid understanding of the game. After that, as part of the actual game, you will be shown four different networks and you will have to select a set of two circles in each of these networks. After finishing this task, you will be asked to play the second phase of the game. The instructions of the second phase will be shown to you 
after you complete the first phase of the game.</p>


<H2><u>Rewards</u></H2>
<p align="justify">You will only be paid money if you finish the entire game (both phase 1 and phase 2). You will be paid a base compensation of 50 cents for finishing both the phases of this game. In addition, on each of the four networks for which you select circles, you will be paid a bonus amount which will depend on how many youth 
(or circles) were made aware about HIV with your selection. For each youth who was made aware of HIV by your selection, you will earn 1 cent. Thus, in the example shown above, since that random selection was able to make 6 youth aware of HIV, selecting that random choice will allow you to win 6 cents for that network. Thus, in order to win more money, you
should aim to select circles which will spread awareness about HIV to the most number of youth. You will also get a bonus 50 cents for finishing the second phase of the game.</p>
<!--<h4>I have read the study instructions.</h4>

	<input type="radio" name="formAgreement" id= "formAgreementTrue" value="Yes" onClick="EnableSubmit()"> Yes<br>
	<input type="radio" name="formAgreement"  id= "formAgreementfalse" value="No"  onClick="EnableSubmit()" > No<br>
	<br>    
</body>
 <script  type="text/javascript">
     function EnableSubmit()
        {
            var submit = document.getElementById("submit");
            var val=  document.getElementById("formAgreementTrue");
            if (  val.checked == true)
            {
                submit.disabled = false;
            }
            else
            {
                submit.disabled = true;
            }
        } 
 </script>

 <input type="submit" name="submit" id="submit" value="<?php echo $BtnLabel; ?>"   disabled = true/>
    </form>-->
</html>
<? ob_flush(); ?>




















